package com.cy.computerstore.mapper;

import com.cy.computerstore.entity.Address;
import com.cy.computerstore.entity.Order;
import com.cy.computerstore.entity.OrderItem;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author diao 2022/4/3
 */
@SpringBootTest
public class OrderMapperTests {
    @Autowired
    private OrderMapper orderMapper;

    /**
     * 插入详细数据到订单表中
     * 前提：订单所属用户(id,名字，电话)，订单id(主键)
     */
    @Test
    public void insertOrder(){
        Order order = new Order();
        order.setUid(8);
        order.setRecvName("Fairy同学");
        order.setRecvPhone("18175143063");
        orderMapper.insertOrder(order);
    }

    /**
     * 将订单详细插入到归属表Order_item中
     * 前提：非空的字段记得一定要封装到OrderItem中
     * 哪个订单(oid),商品的id，以及商品名字啥的
     */
    @Test
    public void insertOrderItem(){
        OrderItem orderItem = new OrderItem();
        orderItem.setOid(3);
        orderItem.setPid(10000004);
        orderItem.setTitle("计算器");
        orderMapper.insertOrderItem(orderItem);
    }
}
