package com.cy.computerstore.mapper;

import com.cy.computerstore.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

/**
 * @author diao 2022/3/21
 */
//@SpringBootTest表示这个类是一个测试类（特点：不会随同项目打包操作）
@SpringBootTest

public class UserMapperTests {
//idea有自动检测功能，接口被mybatis动态代理实现放入容器了；
    @Autowired
    private UserMapper userMapper;

    /**单元测试方法可以独立运行，提高代码运行效率
     * 1.必须被@Test修饰
     * 2.返回值必须是void
     * 3.方法的参数列表不指定任何类型
     * 4.访问修饰符必须是public
     */
    @Test
    public void insert(){
        User user = new User();
        user.setUsername("Fairy");
        user.setPassword("2002514wyh11");

        Integer rows = userMapper.insert(user);
        System.out.println(rows);
    }

    @Test
    public void findByUsername(){
        User user = userMapper.findByUsername("Fairy");
//        打印user信息
        System.out.println(user);
    }

    @Test
    public void updatePasswordByUid(){
        userMapper.updatePasswordByUid(7,"123",
                "管理员",new Date());
    }

    @Test
    public void findByUid(){
        System.out.println(userMapper.findByUid(7));
    }

    @Test
    public void updateInfoByUid(){
        //1.将uid，phone，email，gender，modifiedUser，modifiedTime封装到user中
        User user = new User();
        user.setUid(7);
        user.setPhone("18175143063");
        user.setEmail("746879613@qq.com");
        user.setGender(1);
        user.setModifiedUser("系统管理员");
        user.setModifiedTime(new Date());

        //2.调用userMapper底层方法进行修改用户信息
        Integer rows = userMapper.updateInfoByUid(user);
        System.out.println("rows="+rows);
    }

    @Test
    public void updateAvatarByUid(){
        userMapper.updateAvatarByUid(
                8,
                "/upload/avatar.png",
                "管理员",
                new Date()
        );
    }
}
