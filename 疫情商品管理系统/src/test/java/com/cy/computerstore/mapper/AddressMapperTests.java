package com.cy.computerstore.mapper;

import com.cy.computerstore.entity.Address;
import com.cy.computerstore.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;

/**
 * @author diao 2022/3/21
 */
//@SpringBootTest表示这个类是一个测试类（特点：不会随同项目打包操作）
@SpringBootTest

public class AddressMapperTests {
//idea有自动检测功能，接口被mybatis动态代理实现放入容器了；
    @Autowired
    private AddressMapper addressMapper;

    //测试插入，测试字段为不为null的就行，可以为null的你测不测都无所谓
    @Test
    public void insert(){
        Address address = new Address();
        address.setUid(22);
        address.setPhone("18175143063");
        address.setName("女朋友");
        addressMapper.insert(address);
    }

    @Test
    public void countByUid(){
        Integer integer = addressMapper.countByUid(22);
        System.out.println(integer);
    }

    @Test
    public void findByUid(){
        List<Address> list = addressMapper.findByUid(8);
        System.out.println(list);
    }

    @Test
    public void findByAid(){
        Address aid = addressMapper.findByAid(8);
        System.out.println(aid);
    }

    @Test
    public void updateNonDefault(){
//     将用户的地址全部设置为默认
        addressMapper.updateNonDefault(8);
    }

    @Test
    public void update(){
        addressMapper.updateDefaultByAid(8,"Wuyuhang",new Date());
    }

    @Test
    public void deleteByAid(){
        addressMapper.deleteByAid(6);
    }

    @Test
    public void findLastModified(){
        System.out.println(addressMapper.findLastModified(8));
    }
}

