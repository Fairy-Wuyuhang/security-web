package com.cy.computerstore.mapper;

import com.cy.computerstore.entity.Address;
import com.cy.computerstore.entity.District;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author diao 2022/3/21
 */
//@SpringBootTest表示这个类是一个测试类（特点：不会随同项目打包操作）
@SpringBootTest

public class DistrictMapperTests {
//idea有自动检测功能，接口被mybatis动态代理实现放入容器了；
    @Autowired
    private DistrictMapper districtMapper;

    @Test
    public void findByParent(){
        //根据父区域代码查询下面区域的列表
        List<District> list = districtMapper.findByParent("610000");
        for (District district : list) {
            System.out.println(district);
        }
    }
//根据省市区的code查询它们的名字
    @Test
    public void findNameByCode(){
        String name = districtMapper.findNameByCode("610000");
        System.out.println(name);
    }
}
