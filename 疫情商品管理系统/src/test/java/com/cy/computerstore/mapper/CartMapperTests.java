package com.cy.computerstore.mapper;

import com.cy.computerstore.entity.Address;
import com.cy.computerstore.entity.Cart;
import com.cy.computerstore.vo.CartVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;

/**
 * @author diao 2022/3/21
 */
//@SpringBootTest表示这个类是一个测试类（特点：不会随同项目打包操作）
@SpringBootTest

public class CartMapperTests {
//idea有自动检测功能，接口被mybatis动态代理实现放入容器了；
    @Autowired
    private CartMapper cartMapper;

    /*插入一个数据*/
    @Test
    public void insert(){
        Cart cart = new Cart();
        cart.setUid(8);
        cart.setPid(10000011);//商品id
        cart.setNum(2);
        cart.setPrice(1000L);
        cartMapper.insert(cart);
    }

    /*根据cid购物车的id修改购物车数据*/
    @Test
    public void updateNumByCid(){
        cartMapper.updateNumByCid(1,3,"Fairy同学",new Date());
    }

    /*根据uid以及对应的商品id得到购物车商品信息*/
    @Test
    public void findByUidAndPid(){
        Cart cart = cartMapper.findByUidAndPid(8, 10000011);
        System.out.println(cart);
    }

    /**
     * 查询自定义数据
     */
    @Test
    public void findVOByUid(){
        List<CartVO> list = cartMapper.findVOByUid(8);
        System.out.println(list);
    }

    @Test
    public void findByCid(){
        Cart cart = cartMapper.findByCid(1);
        System.out.println(cart);
    }

    /*测试根据cid得到购物车中的商品信息
    * */
    @Test
    public void findVOByCid(){
        Integer[]cids={1,2,3,4};
        System.out.println(cartMapper.findVOByCid(cids));
    }

    @Test
    public void deleteByCid(){
        Integer rows = cartMapper.deleteByCid(4);
        System.out.println(rows);
    }
}

