package com.cy.computerstore.service;

import com.cy.computerstore.entity.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author diao 2022/4/4
 */
@SpringBootTest
public class OrderServiceTests {
   @Autowired
    private IOrderService orderService;

   @Test
   public void create(){
       Integer[] cids={5,6};
       Order test02 = orderService.create(12, 8, "test02", cids);

       System.out.println(test02);
   }
}
