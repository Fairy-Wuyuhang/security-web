package com.cy.computerstore.service;

import com.cy.computerstore.entity.Address;
import com.cy.computerstore.entity.User;
import com.cy.computerstore.service.ex.ServiceException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author diao 2022/3/22
 */
@SpringBootTest
public class AddressServiceTests {

    @Autowired
    private IAddressService addressService;

    @Test
    public void addNewAddress(){
        Address address = new Address();
        //1.封装地址信息
        address.setPhone("110");
        address.setName("男朋友");
        addressService.addNewAddress(8,"管理员",address);
    }

    @Test
    public void setDefault(){
        addressService.setDefault(11,8,"管理员");
    }

    @Test
    public void delete(){
        addressService.delete(1,23,"管理员");
    }

}
