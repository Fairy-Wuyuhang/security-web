package com.cy.computerstore.service;

import com.cy.computerstore.entity.User;
import com.cy.computerstore.service.ex.ServiceException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author diao 2022/3/22
 */
@SpringBootTest
public class UserServiceTests {

    @Autowired
    private IUserService userService;

    @Test
    public void reg(){
        try {
            User user = new User();
            user.setUsername("test026");
            user.setPassword("2002514wyh11");
            userService.reg(user);
            System.out.println("ok");
        } catch (ServiceException e) {//因为业务层可能会抛出异常
            //获取类的对象再获取类的名称
            System.out.println(e.getClass().getSimpleName());
            //获取异常类的信息
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void login(){
        try {
            User user = userService.login("test01", "123");
            System.out.println(user);
        } catch (ServiceException e) {
            //获取异常信息
            System.out.println(e.getClass().getSimpleName());
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void changePassword(){
       userService.changePassword(8,"test02","2002514wyh11","123");
    }

    //通过uid获取个人资料用户数据
    @Test
    public void getByUid(){
        User user = userService.getByUid(8);
        System.out.println(user);
    }

    @Test
    public void changeInfo(){
        //1.创建一个User对象，里面封装个人资料数据
        User user = new User();
        user.setPhone("18175143063");
        user.setEmail("746879613@qq.com");
        user.setGender(0);

        userService.changeInfo(8,"test02",user);
    }

    @Test
    public void changeAvatar(){
        userService.changeAvatar(8,"/upload/test.png","小明");
    }

}
