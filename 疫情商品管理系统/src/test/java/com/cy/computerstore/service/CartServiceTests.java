package com.cy.computerstore.service;

import com.cy.computerstore.entity.Address;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author diao 2022/3/22
 */
@SpringBootTest
public class CartServiceTests {

    @Autowired
    private ICartService cartService;

    /**
     * 添加商品，amount：添加的个数
     */
    @Test
    public void addToCart(){
        cartService.addToCart(8,10000013,3,"Fairy同学");
    }
    @Test
    public void addToCart1(){
        cartService.addToCart(8,10000013,6,"Fairy同学");
    }

    @Test
    public void addNum(){
        Integer num = cartService.addNum(1, 8, "Fairy同学");
        System.out.println(num);
    }
}
