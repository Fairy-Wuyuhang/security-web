package com.cy.computerstore;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.sql.DataSource;
import java.sql.SQLException;

@SpringBootTest
class ComputerStoreApplicationTests {
    @Autowired
    private DataSource dataSource;

    @Test
    void contextLoads() {
    }

    /*测试数据源
    * HikariProxyConnection@83532470 wrapping com.mysql.cj.jdbc.ConnectionImpl@2d3ef181
    * Hikar底层用的还是c3p0
    * */
    @Test
    void getConnection(){
        try {
            System.out.println(dataSource.getConnection());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
