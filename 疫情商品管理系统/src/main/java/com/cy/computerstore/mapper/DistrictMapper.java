package com.cy.computerstore.mapper;

import com.cy.computerstore.entity.District;

import java.util.List;

/**
 * @author diao 2022/3/29
 */
public interface DistrictMapper {
    /**
     * 根据父代号查询区域信息
     * @param parent 父代号(varchar)
     * @return 某个父区域下的所有区域列表
     */
    List<District> findByParent(String parent);

    /**
     * 根据当前code进行查询省市区名称，对应mapper就是一个查询语句
     * @param code
     * @return
     */
    String findNameByCode(String code);
}
