package com.cy.computerstore.mapper;

import com.cy.computerstore.entity.Address;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @author diao 2022/3/28
 */
/*收货地址持久层的接口*/
public interface AddressMapper {
    /**
     * 插入用户的收货地址数据
     * @param address 收货地址数据
     * @return
     */
    Integer insert(Address address);

    /**
     * 根据用户的id统计收货地址数据
     * @param uid 用户id
     * @return 用户收货地址总数
     */
    Integer countByUid(Integer uid);

    /**
     * 根据用户id查询用户收货地址数据
     * @param uid
     * @return 收货地址数据
     */
    List<Address> findByUid(Integer uid);

    /**
     * 根据aid获取地址
     * @param aid 收货地址id
     * @return 收货地址数据
     */
    Address findByAid(Integer aid);

    /**
     * 根据用户的uid来修改用户的收货地址——>非默认
     * @param uid
     * @return 受影响行数
     */
    Integer updateNonDefault(Integer uid);

    Integer updateDefaultByAid(@Param("aid") Integer aid,
                               @Param("modifiedUser") String modifiedUser,
                               @Param("modifiedTime") Date modifiedTime);


    /**
     * 根据收货地址的id删除地址数据
     * @param aid 收货地址的id
     * @return 受影响的行数
     */
    Integer deleteByAid(Integer aid);

    /**
     * 得到删除后最新时间更新的数据
     * 按modifiedTime排序，limit 0,1
     * @param uid
     * @return
     */
    Address findLastModified(Integer uid);
}
