package com.cy.computerstore.mapper;

import com.cy.computerstore.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * @author diao 2022/3/20
 */
/*用户模块持久层接口*/
public interface UserMapper {
    //这里用包装类可以利用它的api进行业务判断：比如根据插入条数判断成功之类的
    /**
     * 插入用户的数据
     * @param user 用户的数据
     * @return 受影响的行数(增删改影响的行数作为返回值，根据返回值进行判断是否成功)
     */
    Integer insert(User user);

    /**
     * 根据用户名查询用户数据
     * @param username 用户名
     * @return 如果找到对应用户就返回这个用户数据，否则返回null值
     */
    User findByUsername(String username);

    /**
     * 根据uid来修改用户密码
     * @param uid
     * @param password：用户输入的新密码
     * @param modifiedUser：修改的执行者
     * @param modifiedTime：修改数据的时间
     * @return
     */
    Integer updatePasswordByUid(@Param("uid") Integer uid,
                                @Param("password") String password,
                                @Param("modifiedUser") String modifiedUser,
                                @Param("modifiedTime") Date modifiedTime
    );

    /**
     * 更改密码首先要判断该用户是否存在，不排除管理员误删的情况
     * @param uid 用户id
     * @return 返回对象
     */
    User findByUid(Integer uid);

    /**
     * 更改用户信息
     * @param user:user对象(用户数据)
     * @return：返回整数条数
     */
    Integer updateInfoByUid(User user);

    /**
     * @Param("SQL映射文件#{}占位符的变量名")
     * 和映射的接口的参数名不一致时，需要将某个参数强行注入到门口个占位符变量上
     * 根据用户的uid值修改用户头像
     * @param uid
     * @param avatar
     * @param modifiedUser
     * @param modifiedTime
     * @return
     */
    Integer updateAvatarByUid(
            @Param("uid") Integer uid,
            @Param("avatar") String avatar,
            @Param("modifiedUser") String modifiedUser,
            @Param("modifiedTime") Date modifiedTime);
}
