package com.cy.computerstore.mapper;

import com.cy.computerstore.entity.Order;
import com.cy.computerstore.entity.OrderItem;

/**
 * @author diao 2022/4/3
 */
/*订单的持久层接口*/
public interface OrderMapper {

    /**
     * 插入订单数据
     * @param order：订单数据
     * @return：受影响行数
     */
    Integer insertOrder(Order order);

    /**
     * 插入订单项数据到归属表中
     * @return 影响行数
     */
    Integer insertOrderItem(OrderItem orderItem);
}
