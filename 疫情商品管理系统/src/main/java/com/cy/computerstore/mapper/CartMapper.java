package com.cy.computerstore.mapper;

import com.cy.computerstore.entity.Cart;
import com.cy.computerstore.vo.CartVO;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @author diao 2022/4/2
 */
public interface CartMapper {
    /**
     * 插入购物车数据
     * @param cart 购物车数据
     * @return 影响行数
     */
    Integer insert(Cart cart);

    /**
     * 更新购物车中某件商品的数量
     * @param cid 购物车数据id
     * @param num 更新的数量
     * @param modifiedUser
     * @param modifiedTime
     * @return
     */
    Integer updateNumByCid(@Param("cid") Integer cid,
                           @Param("num") Integer num,
                           @Param("modifiedUser") String modifiedUser,
                           @Param("modifiedTime") Date modifiedTime);

    /**
     * 根据用户的id和商品的id来查询购物车中的数据
     * @param uid 用户id
     * @param pid 商品id
     * @return
     */
    Cart findByUidAndPid(@Param("uid") Integer uid,
                         @Param("pid") Integer pid);

    /**
     * 根据uid查询用户购物车中的数据
     * @param uid
     * @return 自定义的CartVO
     */
    List<CartVO> findVOByUid(Integer uid);


    /**
     * 通过cart表中的cid查询购物车中的商品信息
     * @param cid
     * @return
     */
    Cart findByCid(Integer cid);

    /**
     * 根据cid删除购物车中商品信息
     * @param cid
     * @return
     */
    Integer deleteByCid(Integer cid);


    /**
     * 根据购物车中具体的商品id得到商品信息
     * @param cid 商品id
     * @return 商品信息
     */
    List<CartVO> findVOByCid(Integer[] cid);
}
