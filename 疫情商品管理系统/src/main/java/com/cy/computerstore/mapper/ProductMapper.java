package com.cy.computerstore.mapper;

import com.cy.computerstore.entity.Product;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author diao 2022/4/1
 */
public interface ProductMapper {
    //商品列表
    List<Product> findHotList();

    //根据商品id查询商品详情
    Product findById(Integer id);
}
