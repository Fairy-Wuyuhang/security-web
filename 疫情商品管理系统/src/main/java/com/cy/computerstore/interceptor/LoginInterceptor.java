package com.cy.computerstore.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author diao 2022/3/24
 */
/*自定义一个拦截器*/
//加载当前的拦截器并且将其注册到容器中
public class LoginInterceptor implements HandlerInterceptor {
    /**
     * 检测session对象中是否含有uid数据，如果没有就是未完成登录，重定向到登录页面
      * @param request 请求对象
     * @param response 响应对象
     * @param handler 处理器(url+controller:作映射的)
     * @return 返回true表示放行当前请求，否则拦截请求
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Object obj = request.getSession().getAttribute("uid");
        if(obj==null) {
            //session中无uid说明未登录，重定向到登录页面
            response.sendRedirect("/web/login.html");
            return false;
        }
        //放行
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
