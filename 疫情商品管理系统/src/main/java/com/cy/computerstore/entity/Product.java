package com.cy.computerstore.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author diao 2022/3/31
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product extends BaseEntity implements Serializable {
 private Integer id;
 private Integer categoryId;
 private String itemType;
 private String title;
 private String sellPoint;
 private Long price;
 private Integer num;
 private String image;
 //商品状态
 private Integer status;
 //售卖了多少个
 private Integer priority;

}
