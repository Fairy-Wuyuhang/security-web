package com.cy.computerstore.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author diao 2022/3/29
 */
/*省市区数据*/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class District {
    private Integer id;
    //代表父区域编码
    private String parent;
    private String code;
    private String name;
}
