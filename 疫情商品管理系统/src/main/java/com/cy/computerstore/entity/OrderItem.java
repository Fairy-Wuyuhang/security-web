package com.cy.computerstore.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author diao 2022/4/3
 */
//订单中的订单项，展示多个订单，订单表
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderItem extends BaseEntity implements Serializable {
  private Integer id;
  private Integer oid;
  private Integer pid;
  private String title;
  private String image;
  private Long price;
  private Integer num;
}
