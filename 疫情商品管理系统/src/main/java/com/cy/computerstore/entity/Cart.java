package com.cy.computerstore.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author diao 2022/4/2
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Cart extends BaseEntity{
    //购物车中的数据
    private Integer cid;
    private Integer uid;
    private Integer pid;
    private Long price;
    private Integer num;
}
