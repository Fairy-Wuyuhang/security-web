package com.cy.computerstore.util;

/**
 * @author diao 2022/3/22
 */

import java.io.Serializable;
/**
 * Json格式的数据进行响应
 * 将状态码、状态描述信息、以及数据封装到一个类中
 * 将这个类作为方法的返回值返回给前端浏览器
 */
public class JsonResult<E> implements Serializable {
    /*状态码*/
    private Integer state;
    /*描述信息*/
    private String message;
    /*数据（类型是不确定的）*/
    private E data;

    public JsonResult() {
    }

    public JsonResult(Integer state) {
        this.state = state;
    }

    //构造一个状态码+数据的对象
    public JsonResult(Integer stat,E data){
        this.state=stat;
        this.data=data;
    }

    //异常信息的捕获，将捕获的信息给到message
    public JsonResult(Throwable e){
       this.message=e.getMessage();
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public E getData() {
        return data;
    }

    public void setData(E data) {
        this.data = data;
    }
}
