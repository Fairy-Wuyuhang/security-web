package com.cy.computerstore;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.util.unit.DataSize;
import org.springframework.util.unit.DataUnit;

import javax.servlet.MultipartConfigElement;
import javax.servlet.annotation.MultipartConfig;

@SpringBootApplication
//指定当前项目中的mapper接口路径位置，在项目启动时，会自动加载所有接口
@MapperScan("com.cy.computerstore.mapper")
public class ComputerStoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(ComputerStoreApplication.class, args);
    }

//    @Bean
//    public MultipartConfigElement getMultipartConfigElement(){
//        //创建一个配置的工厂类对象
//        MultipartConfigFactory factory = new MultipartConfigFactory();
//
//        // 设置创建的对象的相关信息
//        factory.setMaxFileSize(DataSize.of(10,
//                DataUnit.MEGABYTES));
//        factory.setMaxRequestSize(DataSize.of(15,
//                DataUnit.MEGABYTES));
//
//        //通过工厂类创建MultipartConfigElement对象
//        return factory.createMultipartConfig();
//    }
}
