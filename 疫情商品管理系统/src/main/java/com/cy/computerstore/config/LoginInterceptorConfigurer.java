package com.cy.computerstore.config;

import com.cy.computerstore.interceptor.LoginInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author diao 2022/3/25
 */
//实现webMvcConfigurer接口，完成自定义一些功能配置
@Configuration//这个需要注册到容器中需要Configuration注解
public class LoginInterceptorConfigurer implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        //1.创建自定义的拦截器对象
        HandlerInterceptor interceptor = new LoginInterceptor();

        //2.配置白名单，将其放入list集合中
        List<String> patterns = new ArrayList<>();
        patterns.add("/bootstrap3/**");
        patterns.add("/css/**");
        patterns.add("/images/**");
        patterns.add("/js/**");
        patterns.add("/web/register.html");
        patterns.add("/web/login.html");
        patterns.add("/web/index.html");
        patterns.add("/web/product.html");
        patterns.add("/users/reg");
        patterns.add("/users/login");
        //将省市区列表信息放入白名单中
        patterns.add("/districts/**");
        //将商品类的信息放入白名单中
        patterns.add("/products/**");

        //3.完成拦截器的注册:addPathPatterns代表拦截路径，excludePathPatterns代表白名单
        registry.addInterceptor(interceptor)
                .addPathPatterns("/**")
                .excludePathPatterns(patterns);
    }
}
