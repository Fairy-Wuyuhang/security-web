package com.cy.computerstore.service;

import com.cy.computerstore.entity.User;

/**
 * @author diao 2022/3/22
 */
/*用户模块业务层接口*/
public interface IUserService {
    /**
     * 定义一个user类型的操作列表：用户注册方法
     * @param user 用户的数据对象，参数是根据底层dao来的
     */
    void reg(User user);

    /**
     * 登录功能
     * @param username
     * @param password
     * @return 返回一个User对象，因为我们登录之后，页面会有用户信息，比如右上角那种
     * 所以我们在登录成功之后要把当前用户数据以用户对象的形式进行返回
     * 状态管理：我们可以将数据保存在cookie或者session中，可以减少代码冗余
     * 避免重复很高的数据进行频繁的数据操作（比如说登录之后买东西还对数据库进行查询看你地理位置对不对之类的）
     * 一些常用的数据已经保存起来
     */
    User login(String username,String password);

    /**
     * 修改密码
     * @param uid
     * @param username
     * @param oldPassword
     * @param newPassword
     */
    void changePassword(Integer uid,
                        String username,
                        String oldPassword,
                        String newPassword);


    /**
     * 获取当前登录的用户信息
     * @param uid
     * @return 返回当前用户信息展示到页面表单中
     * 声明一下，其实在我们登录之后，可以将用户信息放入到session中进行调用
     * 也可以获取我们需要的信息展示出来，但是session过期了就另外一码事了
     * 问题又来了，过期了不直接重新登录嘛
     */
    User getByUid(Integer uid);


    /**
     * 修改用户资料
     * @param uid 当前登录的用户id：uid和username都是可以从session中获取
     *            在你登录的时候，uid和username就在session中
     * @param username 当前登录的用户名
     * @param user 修改用户对象：接收客户端个人资料能够提交的数据（用户phone,性别...）
     */
    void changeInfo(Integer uid,String username,User user);

    /**
     * 修改用户头像
     * @param uid 用户id
     * @param avatar 用户头像路径
     * @param username 用户名称
     */
    void changeAvatar(Integer uid,String avatar,String username);
}
