package com.cy.computerstore.service;

import com.cy.computerstore.entity.District;

import java.util.List;

/**
 * @author diao 2022/3/29
 */
public interface IDistrictService {

    /**
     * 根据父代号查询所有区域信息
     * @param parent
     * @return 多个区域信息
     */
    List<District> getByParent(String parent);

    /**
     * 根据code查询省市区名字
     * @param code
     * @return
     */
    String getNameByCode(String code);
}