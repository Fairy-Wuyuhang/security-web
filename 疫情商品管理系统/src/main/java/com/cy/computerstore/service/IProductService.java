package com.cy.computerstore.service;

import com.cy.computerstore.entity.Product;

import java.util.List;

/**
 * @author diao 2022/4/1
 */
public interface IProductService {
    public List<Product> findHotList();

    public Product findById(Integer id);
}
