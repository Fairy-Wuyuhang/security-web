package com.cy.computerstore.service.impl;

import com.cy.computerstore.entity.Cart;
import com.cy.computerstore.entity.Product;
import com.cy.computerstore.mapper.CartMapper;
import com.cy.computerstore.mapper.ProductMapper;
import com.cy.computerstore.service.ICartService;
import com.cy.computerstore.service.ex.CartNotFoundException;
import com.cy.computerstore.service.ex.InsertException;
import com.cy.computerstore.service.ex.UpdateException;
import com.cy.computerstore.util.JsonResult;
import com.cy.computerstore.vo.CartVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * @author diao 2022/4/2
 */
@Service
public class CartServiceImpl implements ICartService {

   @Autowired
   private CartMapper cartMapper;
   @Autowired
   private ProductMapper productMapper;

    /**
     *添加到购物车：里面有好几种方法：
     * 1.购物车cart表中是否存在该数据，不是，就insert
     * 2.如果cart表中已经存在过，你还要添加到购物车，那么就是update商品信息
     * @param uid 用户id
     * @param pid 商品id
     * @param amount 新增数量(前端)
     * @param username 用户名
     */
    @Override
    public void addToCart(Integer uid, Integer pid,
                          Integer amount, String username) {
        //1.首先判断要添加的这个购物是否存在
        Cart result = cartMapper.findByUidAndPid(uid, pid);

        if(result==null){
            //表示是第一次插入
            Cart cart = new Cart();
            cart.setUid(uid);
            cart.setPid(pid);
            cart.setNum(amount);

            //补：价格：来自商品中的数据(根据商品id得到商品中的数据)
            Product product = productMapper.findById(pid);
            cart.setPrice(product.getPrice());

            //2.补充日志
            cart.setCreatedTime(new Date());
            cart.setCreatedUser(username);
            cart.setModifiedTime(new Date());
            cart.setModifiedUser(username);

            //3.执行数据插入
            Integer rows = cartMapper.insert(cart);

            //4.判断可能出现的异常
            if(rows!=1){
                throw new InsertException("插入数据时产生异常");
            }
            /*当前商品已经在购物车存在*/
        }else{
            //5.因为已经有商品了，所以我们将其属性进行更新
            /*将原有数据与现有传递的数据进行相加操做*/
            Integer num = result.getNum() + amount;

            Integer rows = cartMapper.updateNumByCid(
                    result.getCid(),
                    num,
                    username,
                    new Date()
            );

            if(rows!=1){
                throw new UpdateException("更新数据时产生异常");
            }
        }
    }

    /**
     * 用户购物车列表展示业务实现
     * @param uid 用户id
     * @return 用户的购物车商品信息
     */
    @Override
    public List<CartVO> getVOByUid(Integer uid) {

        return cartMapper.findVOByUid(uid);

    }

    /**
     * 添加新的数据在购物车中
     * @param cid：购物车中商品id
     * @param uid：用户id
     * @param username
     * @return：返回购物车中数据总量
     */
    @Override
    public Integer addNum(Integer cid, Integer uid, String username) {
        //1.首先查找购物车中是否含有该数据
        Cart result = cartMapper.findByCid(cid);
        if(result==null){
            throw new CartNotFoundException("购物车中商品数据不存在");
        }

        //2.有该数据则对数据进行更新
        Integer num = result.getNum() + 1;

        Integer rows = cartMapper.updateNumByCid(cid, num, username, new Date());

        if(rows!=1){
            throw new UpdateException("更新数据失败");
        }
        //3.返回新的购物车数据的总量
        return num;
    }


    /**
     * 将购物车中商品数据减少
     * @param cid
     * @param uid
     * @param username
     * @return
     */
    @Override
    public Integer subNum(Integer cid, Integer uid, String username) {
        Cart result = cartMapper.findByCid(cid);
        Integer num=null;
        if(result==null){
            throw new CartNotFoundException("购物车中商品数据不存在");
        }
        //对数据进行更新
        if(result.getNum()>=1){
          num=result.getNum()-1;
        }else{
            throw new UpdateException("商品数量已经不能再减少");
        }
        Integer rows = cartMapper.updateNumByCid(cid, num, username, new Date());
        return rows;
    }


    /**
     * 根据cid删除购物车中的商品信息
     * @param cid
     * @param uid
     * @param username
     */
    @Override
    public void deleteByCid(Integer cid, Integer uid, String username) {
        Cart result = cartMapper.findByCid(cid);
        if(result==null){
            throw new CartNotFoundException("购物车商品数据找不到");
        }

        Integer rows = cartMapper.deleteByCid(cid);
        if(rows!=1){
            throw new UpdateException("数据修改失败");
        }
    }


    /**
     *
     * @param uid ：购物车中商品的归属，属于哪一个用户
     * @param cids：商品id
     * @return:商品数据
     */
    @Override
    public List<CartVO> findVOByCid(Integer uid, Integer[] cids) {
        List<CartVO> list = cartMapper.findVOByCid(cids);
        Iterator<CartVO> item = list.iterator();

        //对迭代器元素进行遍历，看商品的归属是不是uid
      while(item.hasNext()){
          CartVO cartVO = item.next();
          if(!cartVO.getUid().equals(uid)){
              //如果集合中有归属不一致的元素，就将其删除
              list.remove(cartVO);
          }
      }
      return list;
    }




}
