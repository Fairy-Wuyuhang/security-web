package com.cy.computerstore.service;

import com.cy.computerstore.vo.CartVO;

import java.util.List;

/**
 * @author diao 2022/4/2
 */
public interface ICartService {

    /**
     * 将商品添加到购物车中
     * @param uid 用户id
     * @param pid 商品id
     * @param amount 新增数量
     * @param username 用户名
     */
    void addToCart(Integer uid,Integer pid,Integer amount,String username);


    List<CartVO> getVOByUid(Integer uid);


    /**
     * 更新用户购物车数据的数量
     * @param cid
     * @param uid
     * @param username
     * @return 返回新的购物车数据总量
     */
    Integer addNum(Integer cid,Integer uid,String username);

    /**
     * 将购物车中的商品数量进行更新
     * @param cid
     * @param uid
     * @param username
     * @return
     */
    Integer subNum(Integer cid,Integer uid,String username);

    void deleteByCid(Integer cid,Integer uid,String username);

    /**
     * 拿到购物车中的商品信息
     * @param uid ：购物车中商品的归属，属于哪一个用户
     * @param cids：商品id
     * @return
     */
    List<CartVO> findVOByCid(Integer uid,Integer[] cids);
}
