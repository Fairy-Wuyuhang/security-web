package com.cy.computerstore.service.impl;

import com.cy.computerstore.entity.Address;
import com.cy.computerstore.entity.Order;
import com.cy.computerstore.entity.OrderItem;
import com.cy.computerstore.mapper.OrderMapper;
import com.cy.computerstore.service.IAddressService;
import com.cy.computerstore.service.ICartService;
import com.cy.computerstore.service.IOrderService;
import com.cy.computerstore.service.ex.InsertException;
import com.cy.computerstore.vo.CartVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author diao 2022/4/4
 */
@Service
public class OrderServiceImpl implements IOrderService {
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private IAddressService addressService;
    @Autowired
    private ICartService cartService;

    /**
     * @param aid：地址id
     * @param uid：订单所属用户
     * @param username：用户名
     * @param cids：订单中的购物车商品
     * @return
     */
    @Override
    public Order create(Integer aid, Integer uid, String username, Integer[] cids) {
        //1.得到购物车选中的商品信息
        List<CartVO> list = cartService.findVOByCid(uid,cids);

        //2.计算商品总价
        Long totalPrice=0L;
        for (CartVO c : list) {
            totalPrice += c.getRealPrice() * c.getNum();
        }

        //3.获取地址信息
        Address address = addressService.getByAid(aid, uid);
        Order order = new Order();

        order.setUid(uid);
        order.setRecvName(address.getName());
        order.setRecvPhone(address.getPhone());
        order.setRecvProvince(address.getProvinceName());
        order.setRecvCity(address.getCityName());
        order.setRecvArea(address.getAreaName());
        order.setRecvAddress(address.getAddress());

        //4.设置支付的总价以及状态
        order.setStatus(0);
        order.setTotalPrice(totalPrice);
        order.setOrderTime(new Date());

        //5.日志
        order.setCreatedUser(username);
        order.setCreatedTime(new Date());
        order.setModifiedUser(username);
        order.setModifiedTime(new Date());

        //将order插入到订单表中
        Integer rows = orderMapper.insertOrder(order);
        if(rows!=1){
            throw new InsertException("插入订单失败");
        }

        //订单详细数据
        for (CartVO c : list) {
            //创建订单数据对象
            OrderItem orderItem = new OrderItem();
            orderItem.setOid(order.getOid());
            orderItem.setPid(c.getPid());
            orderItem.setTitle(c.getTitle());
            orderItem.setImage(c.getImage());
            orderItem.setPrice(c.getRealPrice());
            orderItem.setNum(c.getNum());
            //日志字段
            orderItem.setCreatedUser(username);
            orderItem.setCreatedTime(new Date());
            orderItem.setModifiedUser(username);
            orderItem.setModifiedTime(new Date());
            //插入数据操作
            rows = orderMapper.insertOrderItem(orderItem);

        if(rows!=1){
            throw new InsertException("插入数据产生异常");
        }
        }

        return order;
    }
}
