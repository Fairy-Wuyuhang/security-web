package com.cy.computerstore.service.impl;

import com.cy.computerstore.entity.District;
import com.cy.computerstore.mapper.DistrictMapper;
import com.cy.computerstore.service.IDistrictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author diao 2022/3/29
 */
@Service
public class DistrictServiceImpl implements IDistrictService {
   @Autowired
   private DistrictMapper districtMapper;

    @Override
    public List<District> getByParent(String parent) {
        List<District> list = districtMapper.findByParent(parent);
        /**网络传输数据中
         *将无效数据设置为null——>节省流量，提升了效率速度
         */
        for (District d : list) {
            d.setId(null);
            d.setParent(null);
        }
        return list;
    }

    @Override
    public String getNameByCode(String code) {
        return districtMapper.findNameByCode(code);
    }
}
