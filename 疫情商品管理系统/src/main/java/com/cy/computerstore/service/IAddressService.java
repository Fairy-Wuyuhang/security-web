package com.cy.computerstore.service;

import com.cy.computerstore.entity.Address;

import java.util.List;

/**
 * @author diao 2022/3/28
 */
/*收货地址业务层接口*/
public interface IAddressService {
    //先统计计数，如果计数你的地址>20，就不要插入了，所以业务层来一个方法就行

    /**
     *因为这里实际上是封装了两个方法：1.判断数据量，2.一个插入
     * @param uid：用于查询用户有多少个地址
     * @param username：用于获取修改用户以及时间
     * @param address：地址内容
     */
    void addNewAddress(Integer uid,String username,Address address);

    /**
     * 根据用户的id得到用户的的所有地址
     * @param id
     * @return 地址数据
     */
    List<Address> getByUid(Integer id);

    /**
     * 修改某个用户的某条地址数据为默认收货地址
     * @param aid 收货地址的aid
     * @param uid 用户id
     * @param username
     */
    void setDefault(Integer aid,Integer uid,String username);


    void delete(Integer aid,Integer uid,String username);

    /**
     * 在业务接口中定义根据收货地址的id来获取收货数据
     * @param aid
     * @param uid
     * @return
     */
    Address getByAid(Integer aid,Integer uid);
}
