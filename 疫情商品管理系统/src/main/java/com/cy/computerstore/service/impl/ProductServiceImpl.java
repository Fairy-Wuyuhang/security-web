package com.cy.computerstore.service.impl;

import com.cy.computerstore.entity.Product;
import com.cy.computerstore.mapper.ProductMapper;
import com.cy.computerstore.service.IProductService;
import com.cy.computerstore.service.ex.ProductNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author diao 2022/4/1
 */
@Service
public class ProductServiceImpl implements IProductService {

   @Autowired
   private ProductMapper productMapper;

    @Override
    public List<Product> findHotList() {
        //1.查找热销商品
        List<Product> list = productMapper.findHotList();

        //2.将一些不需要的字段设置为null
        for (Product product : list) {
            product.setPriority(null);
            product.setCreatedTime(null);
            product.setCreatedUser(null);
            product.setModifiedUser(null);
            product.setModifiedTime(null);
        }
        return list;
    }

    /*根据id查找商品*/
    @Override
    public Product findById(Integer id) {
        Product product = productMapper.findById(id);
        //判断查询结果是否为null
        if(product==null){
            throw new ProductNotFoundException("尝试访问的商品数据不存在！");
        }

        //2.将查询结果的部分属性设置为null
        product.setPriority(null);
        product.setCreatedUser(null);
        product.setCreatedTime(null);
        product.setModifiedUser(null);
        product.setModifiedUser(null);

        //3.返回结果
        return product;
    }
}
