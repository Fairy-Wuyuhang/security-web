package com.cy.computerstore.service.ex;

/**
 * @author diao 2022/3/22
 */
/*业务层异常的基类*/
public class ServiceException extends RuntimeException{
    //只抛出异常
    public ServiceException() {
        super();
    }

    /**
     * 还抛出异常提示信息
     * @param message
     */
    public ServiceException(String message) {
        super(message);
    }

    /**
     * @param message：异常提示信息
     * @param cause：抛出异常对象
     */
    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

    protected ServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
