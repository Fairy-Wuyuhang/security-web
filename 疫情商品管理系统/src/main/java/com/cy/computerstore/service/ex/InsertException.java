package com.cy.computerstore.service.ex;

/**
 * @author diao 2022/3/22
 */
public class InsertException extends RuntimeException{
    /**
     * 在数据库中进行操作出现异常
     */
    public InsertException() {
        super();
    }

    public InsertException(String message) {
        super(message);
    }

    public InsertException(String message, Throwable cause) {
        super(message, cause);
    }

    public InsertException(Throwable cause) {
        super(cause);
    }

    protected InsertException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
