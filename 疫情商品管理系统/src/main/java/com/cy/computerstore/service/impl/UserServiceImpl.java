package com.cy.computerstore.service.impl;

import com.cy.computerstore.entity.User;
import com.cy.computerstore.mapper.UserMapper;
import com.cy.computerstore.service.IUserService;
import com.cy.computerstore.service.ex.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.Date;
import java.util.UUID;

/**
 * @author diao 2022/3/22
 */
/*用户模块业务层的实现类*/
 @Service//将当前累的对象交给Spring来管理
public class UserServiceImpl implements IUserService {
   //调用底层mapper实现user插入
    @Autowired
    private UserMapper userMapper;


 /**
  * 注册功能
  * @param user 用户的数据对象，参数是根据底层dao来的
  */
 @Override
    public void reg(User user) {
       //1、先通过传过来的user参数获取username
     String username = user.getUsername();

       //2、通过底层接口方法findByUsername判断用户是否被注册过
     User result = userMapper.findByUsername(username);

       //3.判断是否为null
     if(result!=null){
        //说明注册过了，抛出用户名占用异常
        throw new UsernameDuplicatedException("用户名被占用");
     }

     String oldPassword = user.getPassword();
      //将密码和盐值作为一个整体作为一个整体进行加密
     String salt = UUID.randomUUID().toString().toUpperCase();

     //补全数据：盐值的记录( 因为每次调用这个方法，salt是不一样的),
     // 方便登录，因为你登录的话密码验证只能验证加密后的密码，而盐值又是随机的，所以得保存
     user.setSalt(salt);

     //将获取的老密码以及盐值封装到MD5加密方法中,并且将新密码设置到用户user中
     String md5Password = getMD5Password(oldPassword, salt);
     user.setPassword(md5Password);

     //补全的数据：is_delete 设置为0,不删除，说明不会对用户进行拦截
     user.setIsDelete(0);

     // 补全数据：4个日志字段
     user.setCreatedUser(user.getUsername());
     user.setModifiedUser(user.getUsername());
     Date date = new Date();
     user.setCreatedTime(date);
     user.setModifiedTime(date);


     //4、为null，说明没有被注册过,我们将用户插入
     Integer rows = userMapper.insert(user);

     if(rows!=1){
      throw new InsertException("插入用户信息发生异常");
     }

    }


 /**
  *
  * @param username:先验证用户名，如果不存在就抛出异常
  * @param password：验证密码：1.先从数据库中取得老密码，和盐值
  *  2.盐值和输入的密码进行MD5加密，最后再与数据库中的老密码进行比对
  * @return
  */
 @Override
   public User login(String username, String password) {
    //1.根据用户名称查询用户数据是否存在，不存在抛出UserNotFoundException
    User result = userMapper.findByUsername(username);
    if(result==null){
       throw new UserNotFoundException("用户名不存在");
    }

    //2.检测密码是否匹配:得到数据库中的老密码
    String oldPassword = result.getPassword();
    String salt = result.getSalt();

    //将输入密码与用户盐值进行相同规则MD5加密
    String newMD5Password = getMD5Password(password, salt);

    //进行比对
    if(!newMD5Password.equals(oldPassword)){
       throw new PasswordNotMatchException("用户密码错误");
    }

   //3.判断is_delete字段是否为1标记已被删除
    if(result.getIsDelete()==1){
       throw new UserNotFoundException("用户数据不存在");
    }

    //4.调用mapper中的查询方法,这里我们进行数据压缩（user里面数据太多了，我们只要需要的几个）
    User user = new User();
    user.setUid(result.getUid());
    user.setUsername(result.getUsername());

    //返回有用户的头像，只要你一登录将avatar信息放入cookie中
    user.setAvatar(result.getAvatar());

    //6.返回已经封装好的User信息
    return user;

    }


 /**
  *
  * @param uid:用户id
  * @param username:用户名
  * @param oldPassword：用户输入的老密码（原密码）
  * @param newPassword：用户输入的新密码
  */
 @Override
 public void changePassword(Integer uid, String username, String oldPassword, String newPassword) {
  User result = userMapper.findByUid(uid);

  //判断是否有用户
  if(result==null||result.getIsDelete()==1){
    throw new UserNotFoundException("用户数据不存在");
  }
  //将用户输入的老密码与数据库中的盐值进行加密与数据库中的实际老密码进行比较
  String md5Password = getMD5Password(oldPassword, result.getSalt());
  if (!result.getPassword().equals(md5Password)){
   throw new PasswordNotMatchException("密码错误");
  }

  //将新的密码设置到数据库中，将新的密码进行加密再去更新
  String newMd5Password = getMD5Password(newPassword, result.getSalt());
  Integer rows = userMapper.updatePasswordByUid(uid, newMd5Password, username, new Date());

  if(rows!=1){
   throw new UpdateException("更新数据时产生异常");
  }
 }


 /**
  * 通过uid获取用户信息
  * @param uid
  * @return 返回用户信息(个人资料所需要的用户信息)
  */
 @Override
 public User getByUid(Integer uid) {
  User result = userMapper.findByUid(uid);
  //对用户信息进行判断，可能出现异常
  if(result==null){
   throw new UserNotFoundException("用户数据不存在");
  }
  if(result.getIsDelete()==1){//可能出现用户被管理员删的情况
   throw new UserNotFoundException("用户不存在");
  }

  //然后我们将个人资料的信息封装到新的User对象中,传输需要的数据通过ajax显示前端上
  User user = new User();
  user.setUsername(result.getUsername());
  user.setPhone(result.getPhone());
  user.setEmail(result.getEmail());
  user.setGender(result.getGender());

  return user;
 }


 /**
  *
  * @param uid 当前登录的用户id
  * @param username 当前登录的用户名
  * @param user 当前用户的新的个人资料数据+时间、修改用户以及uid
  */
 @Override
 public void changeInfo(Integer uid, String username, User user) {
    //调用findByUid()方法，根据Uid查询具体的用户数据（个人资料数据）
  User result = userMapper.findByUid(uid);

    //对数据进行判断，因为你修改是按钮触碰才会发生事件，所以说可能数据已经显示出来了
    //但是你修改确定之后发现原来的数据被管理员删除了，所以这里还需要判断
   if(result==null){
      throw new UserNotFoundException("用户数据不存在");
   }
   if(result.getIsDelete().equals(1)){
    throw new UserNotFoundException("用户数据不存在");
   }

   //向参数user中补全数据：修改时间，修改的人以及uid
   user.setUid(uid);
   user.setModifiedUser(username);
   user.setModifiedTime(new Date());

   //用updateInfoByUid(User user)方法进行修改
  Integer rows = userMapper.updateInfoByUid(user);

  //可能修改数据时候出现异常
  if(rows!=1){
   throw new UpdateException("更新用户数据时出现未知错误，请联系系统管理员");
  }
 }


 @Override
 public void changeAvatar(Integer uid, String avatar, String username) {
   //查询当前用户数据是否存在
  User result = userMapper.findByUid(uid);
   //进行异常判断
  if(result==null||result.getIsDelete().equals(1)){
    throw new UserNotFoundException("用户数据不存在");
  }

  Integer rows = userMapper.updateAvatarByUid(uid, avatar, username, new Date());
  if(rows!=1){
    throw new UpdateException("更新用户头像产生未知的异常");
  }
 }


 /**
  * 定义一个MD5算法加密
  */
 private String getMD5Password(String password,String salt){
  //进行加密（三次）
  for(int i=0;i<3;i++){
   password=DigestUtils.md5DigestAsHex((salt+password+salt).getBytes()).toUpperCase();
  }
  return password;
 }
}
