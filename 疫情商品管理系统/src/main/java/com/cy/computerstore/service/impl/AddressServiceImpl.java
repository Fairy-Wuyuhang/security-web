package com.cy.computerstore.service.impl;

import com.cy.computerstore.entity.Address;
import com.cy.computerstore.mapper.AddressMapper;
import com.cy.computerstore.service.IAddressService;
import com.cy.computerstore.service.IDistrictService;
import com.cy.computerstore.service.ex.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.nio.file.AccessDeniedException;
import java.util.Date;
import java.util.List;

/**
 * @author diao 2022/3/28
 */
@Service
public class AddressServiceImpl implements IAddressService {
   @Autowired
    private AddressMapper addressMapper;

//在添加用户的收货地址的业务层依赖于IDistrictSerevice的业务层接口
   @Autowired
   private IDistrictService districtService;

    @Value("20")
    private Integer maxCount;

    @Override
    public void addNewAddress(Integer uid, String username, Address address) {
        //调用收货地址统计的方法
        Integer count = addressMapper.countByUid(uid);
        //1.进行检测异常
        if(count>maxCount){
            throw new AddressCountLimitException("用户收货地址超出上限");
        }

        /**code是前端传过来的
         * 绑定在address类中
         * 根据这些code封装到address中
         */
        String provinceName = districtService.getNameByCode(address.getProvinceCode());
        String cityName = districtService.getNameByCode(address.getCityCode());
        String areaName = districtService.getNameByCode(address.getAreaCode());
        address.setProvinceName(provinceName);
        address.setCityName(cityName);
        address.setAddress(areaName);
        /**
         * 补全收货地址的一些配置
         */
        //2.将uid添加到address中，目的是一个用户有很多收货地址，这些地址都有一个标识
        address.setUid(uid);

        //3.根据地址数进行判断==0则为默认1，并将默认值封装
        Integer isDefault=count==0?1:0;//1表示默认，0表示不是默认
        address.setIsDefault(isDefault);

        //4.补全4项日志
        address.setCreatedUser(username);
        address.setModifiedUser(username);
        address.setCreatedTime(new Date());
        address.setModifiedTime(new Date());

        //5.插入收货地址的方法，插入的地址会根据前面的isDefalut来判断是否默认
        Integer rows = addressMapper.insert(address);
        if(rows!=1){
            //抛出插入异常
            throw new InsertException("插入数据出现错误");
        }
    }

    /**
     * 根据uid查询地址，uid可以从session中获取，不需要从前端进行传输，数据量比较小
     * GET请求
     * @param uid
     * @return
     */
    @Override
    public List<Address> getByUid(Integer uid) {
        List<Address> list = addressMapper.findByUid(uid);
        for (Address address : list) {
            //有些在前端页面不需要显示的字段设置为null
//            address.setAid(null);
          address.setUid(null);
            address.setProvinceCode(null);
            address.setCityCode(null);
            address.setAreaCode(null);
            address.setTel(null);
           address.setIsDefault(null);
            address.setCreatedTime(null);
            address.setCreatedUser(null);
            address.setModifiedTime(null);
            address.setModifiedUser(null);
        }
        return list;
    }

    /**
     * 设置默认地址——>用户中的某个地址
     * 1.先获取用户在的所有地址
     * 2.然后根据用户id将用户下的所有地址全部非默认
     * 3.最后根据指定的地址id将地址设置为默认
      * @param aid 收货地址的aid（由控制器从前端获得）
     * @param uid 用户id（session中）
     * @param username
     */
    @Override
    public void setDefault(Integer aid, Integer uid, String username) {
       //1.先根据地址的aid查询数据，验证是否出现异常
        Address result = addressMapper.findByAid(aid);
        if(result==null){
            throw new AddressNotFoundException("收货地址不存在");
        }

        //2.可能出现查询出来的地址并不是当前用户的地址
        if(!result.getUid().equals(uid)){
            throw new AddressDeniedException("非法数据访问");
        }

        //3.将用户下所有的地址设置为非默认
        Integer rows = addressMapper.updateNonDefault(uid);
        if(rows<1){
            throw new UpdateException("更新产生未知异常");
        }

        //4.将用户选中的某条地址设置为默认收货地址
        rows=addressMapper.updateDefaultByAid(aid,username,new Date());
        if(rows!=1){
            throw new UpdateException("产生未知异常");
        }
    }


    /**
     *
     * @param aid:删除的记录
     * @param uid：需要再删除之后判断是否数据为0
     * 异常判断，可能出现当前地址的uid与传入uid（也就是session中的）不一致
     * @param username
     */
    @Override
    public void delete(Integer aid, Integer uid, String username)  {
        //1.先对可能出现的异常进行判断,可能通过aid查询到的地址不存在
        Address result = addressMapper.findByAid(aid);
        if(result==null){
            throw new AddressNotFoundException("地址数据找不到!");
        }

        //可能出现用户访问到不属于他的地址（因为uid是登录给的）
        // ，其实我觉得不会出现这种情况，因为你用户的地址展示是根据你uid来的
        if(!result.getUid().equals(uid)){
            throw new AddressDeniedException("非法访问地址");
        }

        //2.删除指定的地址数据
        Integer rows = addressMapper.deleteByAid(aid);

        if(rows!=1){
            throw new DeleteException("删除数据产生未知的异常");
        }

        //3.逻辑判断，删除后是否还有之前的默认地址
        Integer count = addressMapper.countByUid(uid);
        if(count==0) return;

        //3.1如果删除的是默认地址，就进行时间排序，选择最近时间的地址为默认地址
        if(result.getIsDefault()==1){
            Address address = addressMapper.findLastModified(uid);
            rows = addressMapper.updateDefaultByAid(address.getAid(), username, new Date());
        }

        if(rows!=1){
            throw new UpdateException("更新数据时产生未知异常");
        }

    }

    /**
     * 目的是获取地址中一些数据——>订单中需要
     * @param aid 地址id
     * @param uid 用户id
     * @return
     */
    @Override
    public Address getByAid(Integer aid, Integer uid) {
        Address address = addressMapper.findByAid(aid);
        if(address==null){
            throw new AddressNotFoundException("收货地址数据不存在");
        }
        if(!address.getUid().equals(uid)){
            throw new AddressDeniedException("非法地址数据访问");
        }

        //不需要的字段设置为null
        address.setProvinceCode(null);
        address.setCityCode(null);
        address.setAreaCode(null);
        address.setCreatedUser(null);
        address.setCreatedTime(null);
        address.setModifiedUser(null);
        address.setModifiedTime(null);

        return address;
    }


}
