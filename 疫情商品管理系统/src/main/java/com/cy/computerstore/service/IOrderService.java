package com.cy.computerstore.service;

import com.cy.computerstore.entity.Order;

/**
 * @author diao 2022/4/4
 */
public interface IOrderService {

    /**
     * 1.创建订单
     * @param aid：地址id
     * @param uid：订单所属用户
     * @param username：用户名
     * @param aids：订单中的购物车商品
     * @return：订单
     */
      Order create(Integer aid,Integer uid,String username,Integer[] aids);
}
