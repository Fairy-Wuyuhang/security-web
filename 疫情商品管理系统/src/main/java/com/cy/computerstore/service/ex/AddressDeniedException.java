package com.cy.computerstore.service.ex;

/**
 * @author diao 2022/3/30
 */
/*非法访问异常*/
public class AddressDeniedException extends ServiceException{
    public AddressDeniedException() {
        super();
    }

    public AddressDeniedException(String message) {
        super(message);
    }

    public AddressDeniedException(String message, Throwable cause) {
        super(message, cause);
    }

    public AddressDeniedException(Throwable cause) {
        super(cause);
    }

    protected AddressDeniedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
