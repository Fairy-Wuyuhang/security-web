package com.cy.computerstore.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * @author diao 2022/4/5
 */
@Component//将当前类对象创建使用交给spring容器维护
@Aspect
public class TimerAspect {

    /**
     * 切面类中的环绕方法
     * @param pjp：表示连接点，目标类对象的连接点
     * @return
     */
    @Around("execution(* com.cy.computerstore.service.impl.*.*(..))")//切点，位置
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        //2.在目标方法前记录当前时间
        long start = System.currentTimeMillis();
        //1.调用目标方法
        Object result = pjp.proceed();
        //3.记录后面时间
        long end = System.currentTimeMillis();
        System.out.println("耗时"+(end-start));
        return result;
    }
}
