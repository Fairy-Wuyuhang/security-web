package com.cy.computerstore.controller;

import com.cy.computerstore.controller.ex.*;
import com.cy.computerstore.entity.User;
import com.cy.computerstore.service.IUserService;
import com.cy.computerstore.service.ex.InsertException;
import com.cy.computerstore.service.ex.UsernameDuplicatedException;
import com.cy.computerstore.service.impl.UserServiceImpl;
import com.cy.computerstore.util.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author diao 2022/3/22
 */
//这里是返回json数据(被JsonResult类型的)
@RestController
@RequestMapping("users")
public class UserController extends BaseController{

    @Autowired
    private IUserService userService;

    /**1、接收数据方式：请求处理方法的参数列表设置为pojo类型来接收前端数据
     * SpringBoot会将前端的URL地址上的参数名和pojo类的属性名进行比较，如果相同
     * 就会将值注入到pojo类的对应属性上
     * @param user
     * @return
     */
    @RequestMapping("reg")
    public JsonResult<Void> reg(@RequestBody User user){
        userService.reg(user);
        return new JsonResult<Void>(OK);
    }

    /**2、登录功能，每次登录会将数据放入session
     * 接收数据形式：如果参数是非pojo类型
     * 那么SpringBoot会将请求的参数名和方法的参数名直接进行比较，
     * 如果相同则自动注入
     * @param username
     * @param password
     * @return
     */
    @RequestMapping("login")
    public JsonResult<User> login(String username,String password,HttpSession session){
       //1.业务login方法，将封装的user信息放到JsonResult中;
        User data = userService.login(username, password);

        //3.向session对象中完成数据绑定（这里session是全局的）
        session.setAttribute("uid",data.getUid());
        session.setAttribute("username",data.getUsername());

        //4.打印获取session中的数据
        System.out.println(getuidFromSession(session));
        System.out.println(getUsernameFromSession(session));

        //2.将状态码数据封装到JsonResult中
        return new JsonResult<User>(OK,data);
    }


    /**@Session 获取session对象
     * 相当于是一个工具类，获取session中的uid，有利于减少代码冗余
     * @return
     */
    public final Integer getuidFromSession(HttpSession session){
       return Integer.valueOf(session.getAttribute("uid").toString());
    }

    public final String getUsernameFromSession(HttpSession session){
        return session.getAttribute("username").toString();
    }


    /**
     *
     * @param oldPassword
     * @param newPassword
     * @param session
     * @return
     */
    @RequestMapping("change_password")
    public JsonResult<Void> changePassword(String oldPassword,String newPassword,
                                           HttpSession session){
        //uid和username我们直接从session里面获取就好了
        Integer uid = getuidFromSession(session);
        String username = getUsernameFromSession(session);

        //执行业务操作
        userService.changePassword(uid,username,oldPassword,newPassword);
        return new JsonResult<>(OK);
    }


    @RequestMapping("get_by_uid")
    public JsonResult<User> getByUid(HttpSession session){
        User result = userService.getByUid(getuidFromSession(session));
        return new JsonResult<User>(OK,result);
    }

    /**
     * 这个user对象由前端传递
     * @param user
     * @param session
     * @return
     */
    @RequestMapping("change_info")
    public JsonResult<Void> changeInfo(User user, HttpSession session){
        //要完成修改资料需要的数据：username、phone、email、gender
        //uid需要再次封装到user对象中
        Integer uid = getuidFromSession(session);
        String username = getUsernameFromSession(session);
        userService.changeInfo(uid,username,user);
        return new JsonResult<>(OK);
    }

    /*设置上传文件最大值*/
    public static final int AVATAR_MAX_SIZE=10*1024*1024;

    /*限制上传文件的类型*/
    public static final List<String> AVATAR_TYPE=new ArrayList<>();

    static{
        //将文件类型放入集合中
        AVATAR_TYPE.add("image/jpeg");
        AVATAR_TYPE.add("image/png");
        AVATAR_TYPE.add("image/bmp");
        AVATAR_TYPE.add("image/gif");
    }

    /**
     * MultipartFile接口是SpringMvc提供的一个接口，这个接口包装了文件数据（任何类型的file都可以接收）
     * @param session：里面取出username，uid
     * @param file:MultipartFile类型，名字必须是file，前端页面有个name叫file的会自动寻找，
     *            将数据包给到控制层参数名字叫file的
     * @RequestParam("xxx"):与前端表单的数据进行绑定
     * @return
     */
    @RequestMapping("change_avatar")
    public JsonResult<String> changeAvatar(HttpSession session,
                                          @RequestParam("file") MultipartFile file){
        //1.判断文件是否为null
        if(file.isEmpty()){
            throw new FileEmptyException("文件为空");
        }
        if(file.getSize()>AVATAR_MAX_SIZE){
            throw new FileSizeException("文件超出限制,不得超过"+(AVATAR_MAX_SIZE/1024)+"KB的头像文件");
        }

        //2.判断上传的文件类型是否在AVATAR_TYPE里面
        String contentType = file.getContentType();
        System.out.println(contentType);
        if(!AVATAR_TYPE.contains(contentType)){
            throw new FileTypeException("不支持使用该类型文件作为头像,运行文件类型：\n"+AVATAR_TYPE);
        }


        //3.获取当前项目的绝对磁盘路径（就是项目路径）
        String parent = session.getServletContext().getRealPath("upload");
        System.out.println(parent);

        //4.dir表示保存头像文件的文件夹
        File dir = new File(parent);
        if(!dir.exists()){
            //如果不存在的话，就会创建一个
            dir.mkdirs();
        }

        //保存头像文件的文件名
        String originalFilename = file.getOriginalFilename();
        //得到文件后缀名称:先取得.的索引，然后再利用substring
        int beginIndex = originalFilename.lastIndexOf(".");
        String suffix = originalFilename.substring(beginIndex);

        //5.得到全新的文件名
        String filename = UUID.randomUUID().toString() + suffix;

        //6.在指定的目录下创建全新的文件,dest表示保存头像文件
        File dest = new File(dir, filename);

        //7.执行保存的头像文件,将之前的文件中的数据放到新的文件中
        try {
            file.transferTo(dest);
        } catch (IOException e) {
            throw new FileUploadIOException("上传文件时出现读写错误");
        } catch (FileStateException e){
            throw new FileStateException("文件状态异常");
        }

        //8.获取uid以及username和头像路径方便执行业务层的操作
        Integer uid = getuidFromSession(session);
        String username = getUsernameFromSession(session);

        //头像相对路径,作为文件夹upload需要//
        String avatar="/upload/"+filename;
        userService.changeAvatar(uid,avatar,username);

        //9.返回用户头像路径给到前端，用于头像展示
        return new JsonResult<>(OK,avatar);
    }


    /**
     * //需要调用业务层的接口
     *     @Autowired
     *     private IUserService userService;
     *
     *     @RequestMapping("reg")
     *     public JsonResult<Void> reg(User user){
     *         //1、创建响应结果集
     *         JsonResult<Void> result = new JsonResult<>();
     *
     *         //2.进行业务操作，若出现异常设置响应状态码和信息
     *         try {
     *             userService.reg(user);
     *             result.setState(200);
     *             result.setMessage("用户注册成功");
     *         } catch (UsernameDuplicatedException e) {
     *             result.setState(4000);
     *             result.setMessage("用户名被占用");
     *         }catch (InsertException e){
     *             result.setState(5000);
     *             result.setMessage("注册时产生未知异常导致注册失败");
     *         }
     *
     *         //3.返回json格式数据
     *         return result;
     *     }
     */


}
