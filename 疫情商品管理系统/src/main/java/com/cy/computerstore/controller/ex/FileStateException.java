package com.cy.computerstore.controller.ex;

/**
 * @author diao 2022/3/26
 */
//上传文件状态异常（比如说：文件传输的时候，你的文件不能打开，否则出现状态提示错误）
public class FileStateException extends FileUploadException{
    public FileStateException() {
        super();
    }

    public FileStateException(String message) {
        super(message);
    }

    public FileStateException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileStateException(Throwable cause) {
        super(cause);
    }

    protected FileStateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
