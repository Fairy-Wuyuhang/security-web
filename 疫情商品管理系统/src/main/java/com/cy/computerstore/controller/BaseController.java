package com.cy.computerstore.controller;

import com.cy.computerstore.controller.ex.*;
import com.cy.computerstore.entity.Address;
import com.cy.computerstore.service.ex.*;
import com.cy.computerstore.util.JsonResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.nio.file.AccessDeniedException;

/**
 * @author diao 2022/3/23
 */
//这也是处理前端请求的，处理前端抛出的异常，并且将方法返回值传递给前端
@Controller
public class BaseController {
    /*操作成功的状态码*/
    public static final int OK=200;

    /*用于统一处理该异常，方法充当请求处理的方法，返回给到前端*/
    @ExceptionHandler({ServiceException.class,FileUploadException.class})
    public JsonResult<Void> handleException(Throwable e){
        JsonResult<Void> result = new JsonResult<>(e);

        //根据不同异常的抛出，设置不同的响应码和message
        if(e instanceof UsernameDuplicatedException){
            result.setState(4000);
            result.setMessage("用户名已经被占用");
        }else if(e instanceof AddressCountLimitException){
            result.setState(4001);
            result.setMessage("用户收货地址超出上限的异常");
        }else if(e instanceof AddressNotFoundException){
            result.setState(4002);
            result.setMessage("用户的收货地址不存在");
        }else if(e instanceof AccessDeniedException){
            result.setState(4004);
            result.setMessage("非法访问收货地址");
        }else if(e instanceof ProductNotFoundException){
            result.setState(4005);
            result.setMessage("查不到商品数据");
        }else if(e instanceof CartNotFoundException){
            result.setState(4006);
            result.setMessage("购物车中数据不存在异常");
        }else if(e instanceof InsertException){
            result.setState(5000);
            result.setMessage("插入数据时产生未知的异常");
//            关于登录的两个异常
        }else if(e instanceof PasswordNotMatchException){
            result.setState(5002);
            result.setMessage("用户名的密码错误");
        }else if(e instanceof UserNotFoundException){
            result.setState(5001);
            result.setMessage("用户数据不存在异常");
//            更新数据得到异常
        }else if(e instanceof UpdateException){
            result.setState(5001);
            result.setMessage("更新数据时产生未知异常");
        }else if(e instanceof DeleteException){
            result.setState(5002);
            result.setMessage("删除数据时产生未知的异常");
        }else if(e instanceof FileEmptyException){
            result.setState(6000);
        }else if(e instanceof FileSizeException){
            result.setState(6001);
        }else if(e instanceof FileTypeException){
            result.setState(6002);
        }else if(e instanceof FileStateException){
            result.setState(6003);
        }else if(e instanceof FileUploadIOException){
            result.setState(6004);
        }
        return result;
    }
}
