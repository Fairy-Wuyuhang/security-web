package com.cy.computerstore.controller;

import com.cy.computerstore.entity.Order;
import com.cy.computerstore.service.IOrderService;
import com.cy.computerstore.util.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * @author diao 2022/4/4
 */
@RequestMapping("orders")
@RestController
public class OrderController extends BaseController {

    @Autowired
    private IOrderService orderService;

    /**
     * 创建订单(订单详细和订单表)
     * @param aid：地址id
     * @param cids：选择的商品id
     * @param session
     * @return：订单详细表Order（接收人，接收地点...）
     */
    @RequestMapping("create")
    public JsonResult<Order> create(Integer aid, Integer[] cids, HttpSession session) {
        Integer uid = getuidFromSession(session);
        String username = getUsernameFromSession(session);
        Order data = orderService.create(aid, uid, username, cids);
        return new JsonResult<>(OK,data);
    }

    /**
     * 辅助
     * @param session
     * @return
     */
    public static String getUsernameFromSession(HttpSession session){
        return session.getAttribute("username").toString();
    }

    public static Integer getuidFromSession(HttpSession session){
        return Integer.valueOf(session.getAttribute("uid").toString());
    }
}
