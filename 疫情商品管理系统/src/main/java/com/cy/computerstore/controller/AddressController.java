package com.cy.computerstore.controller;

import com.cy.computerstore.entity.Address;
import com.cy.computerstore.service.IAddressService;
import com.cy.computerstore.util.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author diao 2022/3/29
 */
@RequestMapping("address")
@RestController
public class AddressController extends BaseController{

    @Autowired
    private IAddressService addressService;

    /**
     * 返回JsonResult<Void>
     * @param address:地址信息
     * @param session：里面存放uid和username
     * @return
     */
    @RequestMapping("add_new_address")
    public JsonResult<Void> addNewAddress(Address address, HttpSession session){
        Integer uid = getuidFromSession(session);
        String username = getUsernameFromSession(session);

        //1.执行核心添加地址方法，里面包含两个底层接口方法
        addressService.addNewAddress(uid,username,address);
        return new JsonResult<>(OK);
    }

    public final Integer getuidFromSession(HttpSession session){
        return Integer.valueOf(session.getAttribute("uid").toString());
    }

    public final String getUsernameFromSession(HttpSession session){
        return session.getAttribute("username").toString();
    }

    /**
     * 控制器通过session获取uid进行查询得到用户所拥有的地址信息
     * @param session
     * @return
     */
    @RequestMapping({"","/"})
    public JsonResult<List<Address>> getByUid(HttpSession session){
        Integer uid = getuidFromSession(session);
        List<Address> data = addressService.getByUid(uid);
        //将数据回显到前端
        return new JsonResult<>(OK,data);
    }

    /**
     * 设置默认地址
     * @param aid
     * @param session
     * @return
     */
    @RequestMapping("{aid}/set_default")
    public JsonResult<Void> setDefault(@PathVariable("aid") Integer aid,
                                       HttpSession session){
        addressService.setDefault(aid,
                getuidFromSession(session),
                getUsernameFromSession(session));
        return new JsonResult<>(OK);
    }

    @RequestMapping("{aid}/delete")
    public JsonResult<Void> delete(@PathVariable("aid")Integer aid,
                                   HttpSession session){
        addressService.delete(aid,getuidFromSession(session),getUsernameFromSession(session));
        //返回结果集，封装状态码
        return new JsonResult<>(OK);
    }
}
