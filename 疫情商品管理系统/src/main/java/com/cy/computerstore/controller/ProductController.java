package com.cy.computerstore.controller;

import com.cy.computerstore.entity.Product;
import com.cy.computerstore.service.IProductService;
import com.cy.computerstore.util.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author diao 2022/4/1
 */
@RestController
@RequestMapping("products")
public class ProductController extends BaseController{
  @Autowired
  private IProductService productService;

  @RequestMapping("hot_list")
  public JsonResult<List<Product>> getHotList(){
      List<Product> data = productService.findHotList();
      return new JsonResult<List<Product>>(OK,data);
  }

    /**
     * 通过id（从前端获取）得到Product信息
     * @return
     */
  @GetMapping("{id}/details")
  public JsonResult<Product> getById(@PathVariable("id") Integer id){
      //调用业务对象获取数据
      Product data = productService.findById(id);
      return new JsonResult<>(OK,data);
  }

}
