package com.cy.computerstore.controller;

import com.cy.computerstore.entity.District;
import com.cy.computerstore.service.IDistrictService;
import com.cy.computerstore.util.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author diao 2022/3/29
 */
@RequestMapping("districts")
@RestController
public class DistrictController extends BaseController{
    @Autowired
    private IDistrictService districtService;

    /**
     * 根据父区域代码得到他的子区域信息
     * @param parent
     * @return 子区域信息
     */
    @RequestMapping({"/",""})
    public JsonResult<List<District>> getByParent(String parent){
        List<District> data = districtService.getByParent(parent);
        //将区域数据放入JsonResult中，并且放入状态码
        return new JsonResult<>(OK,data);
    }
}
