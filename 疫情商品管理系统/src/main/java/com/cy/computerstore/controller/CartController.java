package com.cy.computerstore.controller;

import com.cy.computerstore.service.ICartService;
import com.cy.computerstore.util.JsonResult;
import com.cy.computerstore.vo.CartVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author diao 2022/4/2
 */
@RestController
@RequestMapping("carts")
public class CartController extends BaseController{
    @Autowired
    private ICartService cartService;


    /**控制器层面只需要传入pid,uid,username,数量即可
     * 将商品信息添加到购物车Cart表中
     * @param pid ：商品id
     * @param amount ：商品数量
     * @param session
     * @return
     */
    @RequestMapping("add_to_cart")
    public JsonResult<Void> addToCart(Integer pid,
                                      Integer amount,
                                      HttpSession session){

        cartService.addToCart(
                getuidFromSession(session),
                pid,
                amount,
                getUsernameFromSession(session));
        return new JsonResult<>(OK);
    }


    /**cart.html
     * 通过uid获取自定义的用户中购物车的数据
     * @param session
     * @return
     */
    @RequestMapping({"","/"})
    public JsonResult<List<CartVO>> getVOByUid(HttpSession session){
           //调用业务方法
        List<CartVO> data = cartService.getVOByUid(getuidFromSession(session));
        //将data封装到JsonResult中返回
        return new JsonResult<>(OK,data);
    }

    /**
     * 在购物车中给商品添加数量
     * @param cid：购物车商品id
     * @param session
     * @return
     */
    @RequestMapping("{cid}/num/add")
    public JsonResult<Integer> addNum(@PathVariable("cid") Integer cid, HttpSession session){
        Integer data = cartService.addNum(
                cid,
                getuidFromSession(session),
                getUsernameFromSession(session)
        );
        return new JsonResult<>(OK,data);
    }

    /**
     * 与上述类似，只不过是减少购物车商品数量
     * @param cid
     * @param session
     * @return
     */
    @RequestMapping("{cid}/num/sub")
    public JsonResult<Integer> subNum(@PathVariable("cid") Integer cid,HttpSession session){
        Integer data = cartService.subNum(
                cid,
                getuidFromSession(session),
                getUsernameFromSession(session)
        );
        return new JsonResult<>(OK,data);
    }

    /**
     * 删除购物车中商品信息
     * @param cid：通过url路径前端传过来的idd
     * @param session
     * @return
     */
    @RequestMapping("{cid}/delete")
    public JsonResult<Void> deleteByCid(@PathVariable("cid") Integer cid,HttpSession session){
        cartService.deleteByCid(
                cid,
                getuidFromSession(session),
                getUsernameFromSession(session)
        );
        return new JsonResult<>(OK);
    }


    @RequestMapping("list")
    public JsonResult<List<CartVO>> getVOByCid(Integer[] cids,HttpSession session){
        List<CartVO> data = cartService.findVOByCid(getuidFromSession(session), cids);
        return new JsonResult<List<CartVO>>(OK,data);
    }


    /**
     * 辅助方法：将session中uid提取出来
     * @param session
     * @return
     */
    public static Integer getuidFromSession(HttpSession session){
      return Integer.parseInt(session.getAttribute("uid").toString());
    }

    public static String getUsernameFromSession(HttpSession session){
      return session.getAttribute("username").toString();
    }

}
