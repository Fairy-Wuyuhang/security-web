package com.wuyuhang.security.controller;

import com.wuyuhang.security.pojo.User;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author diao 2022/2/23
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping("/hello")
    public String hello(){
        return "hello";
    }

    @GetMapping("/index")
    public String index(){
        return "index";
    }


    //专门作为角色的方法
    @GetMapping("/update")
    //设置能够访问的角色
    @Secured({"ROLE_sale","ROLE_manager"})
    public String update(){
        return "update ok";
    }

    @GetMapping("/delete")
    //当你的用户中有admin这个权限就能访问这个方法
    @PreAuthorize("hasAuthority('admins')")
    public String delete(){
        return "delete Ok";
    }

    /*还有一种情况：
    先进行方法处理请求，得到结果
    security验证其结果，也就是说对方法执行完后进行一个校验
    * */
   @GetMapping("/add")
   @PostAuthorize("hasAuthority('teacher')")
   public String add(){
       //方法体中的这个是可以输出的
       System.out.println("开始add...");
       //因为没有该权限，所以无法访问一下内容
       return "hello teacher,add Ok";
   }

   @GetMapping("/getAll")
   @PostAuthorize("hasAnyAuthorize('admins')")
   //PostFilter("filterObject.xxx=='xxx'")会将集合类型中不满足条件的元素进行移除
   @PostFilter("filterObject.username=='admin1'")
  public List<User> getAllUser(){
      ArrayList<User> list = new ArrayList<>();
      list.add(new User(11,"admin1","6666"));
      list.add(new User(12,"admin2","888"));
      System.out.println(list);
      return list;
  }

}
