package com.wuyuhang.security.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author diao 2022/2/24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor

public class User {
    private Integer id;
    private String username;
    private String password;
}
