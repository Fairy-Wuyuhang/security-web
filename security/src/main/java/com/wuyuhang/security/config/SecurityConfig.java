package com.wuyuhang.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfiguration;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

/**
 * @author diao 2022/2/23
 */
//继承WebSecurityAdapter可以得到Security默认的安全功能
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter{

    @Autowired
    private UserDetailsService userDetailsService;
    //注入数据源
    @Autowired
    private DataSource dataSource;

    //配置对象
    @Bean
    public PersistentTokenRepository persistentTokenRepository(){
        JdbcTokenRepositoryImpl jdbcTokenRepository = new JdbcTokenRepositoryImpl();
        jdbcTokenRepository.setDataSource(dataSource);
        return jdbcTokenRepository;
    }

    //configure方法可以配置用户名和密码,并且利用BCryptPasswordEncoder进行密码加密
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(password());
    }

    @Bean
    PasswordEncoder password(){
      return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        //退出，请求logout，成功后转到请求方法处理test/hello
        http.logout().logoutUrl("/logout")
                .logoutSuccessUrl("/test/hello").permitAll();


       //配置没有权限访问403——>跳转到自定义页面
        http.exceptionHandling().accessDeniedPage("/unauth.html");

        http.formLogin()//自定义自己编写的前端页面
                .loginPage("/login.html")//登录页面设置
                .loginProcessingUrl("/user/login")//页面登录表单登录访问路径
                .defaultSuccessUrl("/success.html").permitAll()//登录成功后跳转路径
                .failureUrl("/login?error")
                .and()
                .authorizeRequests()
                .antMatchers("/","/test/hello","/user/login").permitAll()//设置哪些路径可以直接访问，不需要认证
//                .antMatchers("/test/index").hasAnyAuthority("admins,manager")  //只有具有admins权限才能访问这个路径
                .antMatchers("/test/index").hasRole("sale") //根据角色访问
                .anyRequest().authenticated()
                .and()
                .rememberMe().tokenRepository(persistentTokenRepository())
                .tokenValiditySeconds(60)
                .userDetailsService(userDetailsService);
//                .and()
//                .csrf().disable();//关闭csrf防护

//        http.formLogin()
//                .loginPage("/login.html")
//                .loginProcessingUrl("/user/login")
//                .defaultSuccessUrl("/success.html").permitAll()
//                .failureUrl("/login?error")
//                .and()
//                .authorizeRequests()
//                .antMatchers("/test/index").hasRole("sale")
//                .antMatchers("/test/index").hasAnyAuthority("admins,manager")
//                .and()
//                .rememberMe().tokenRepository(persistentTokenRepository())
//

    }
}
