package com.wuyuhang.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author diao 2022/2/23
 */
@ImportResource("UserMapper.xml")
@Configuration
public class UserLoginConfig extends WebSecurityConfigurerAdapter{

//    注入自定义的UserDetailService
    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //在配置方法中，认证管理器auth调用userDetailService方法实现自定义的用户认证
        //并且调用PasswordEncoder方法进行加密；
        auth.userDetailsService(userDetailsService).passwordEncoder(password());
    }

    //需要实现自定义的PasswordEncoder
    @Bean
    PasswordEncoder password(){
        return new BCryptPasswordEncoder();
    }
}
