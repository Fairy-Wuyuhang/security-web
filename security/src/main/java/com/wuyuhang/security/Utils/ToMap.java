package com.wuyuhang.security.Utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author diao 2022/2/24
 */
public class ToMap {
    private Map<String,Object> map=new HashMap<>();

    public ToMap() {

    }

    public ToMap error(){
        map.put("code",Code.ERROR.getCode());
        map.put("msg",Code.ERROR.getMsg());
        return this;
    }

    //加载对象,调用此方法后可以得到该类对象
    public static ToMap getInstance(){
        return new ToMap();
    }

    public ToMap add(String key,Object value){
        map.put(key,value);
        return this;
    }

    public ToMap addUser(String username){
        map.put("username",username);
        return this;
    }

    //传入了一个map，将其放入map中
    public ToMap add(Map<String,Object>paramMap){
        //得到所有传入map的键遍历（entrySet），并将对应的键值对放入全局变量的map中
        for (Map.Entry<String, Object> entry : paramMap.entrySet()) {
            map.put(entry.getKey(),entry.getValue());
        }
        return this;
    }

    public Map<String,Object>getMap(){
        return map;
    }
}
