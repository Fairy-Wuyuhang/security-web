package com.wuyuhang.security.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wuyuhang.security.Utils.ToMap;
import com.wuyuhang.security.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author diao 2022/2/23
 */
//自定义一个名为userDetailsService的组件注入到容器中

@Service("userDetailsService")
public class MyUserDetailService implements UserDetailsService {
@Autowired
private UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userMapper.selectOne(ToMap.getInstance().addUser(username).getMap());

        //进行判断
        if(user==null){
            throw new UsernameNotFoundException("用户名不存在");
        }
        //给数据库中的角色进行授权
        List<GrantedAuthority> authorityList = AuthorityUtils.commaSeparatedStringToAuthorityList("admins,ROLE_sale");
        return new User(user.getUsername(),new BCryptPasswordEncoder().encode(user.getPassword()),authorityList);
    }
}
