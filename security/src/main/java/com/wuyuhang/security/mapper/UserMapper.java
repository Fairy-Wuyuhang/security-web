package com.wuyuhang.security.mapper;

import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * @author diao 2022/2/24
 */
@Repository
//这里我们继承BaseMapper接口，里面CRUD方法都有
public interface UserMapper {
    User selectOne(Map<String,Object>map);
}
