package com.wuyuhang.security.Exception;

import com.wuyuhang.security.Utils.ToMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

/**
 * @author diao 2022/2/25
 */

//全局异常处理,对请求出现的异常进行切面处理（对ArithmeticException和NullPointerxxx处理）
@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    private final String ERROR="error";

  @ExceptionHandler(value=MyException.class)
    public ModelAndView noMyException(MyException e){
      ModelAndView modelAndView = new ModelAndView(ERROR);
      modelAndView.addObject(ERROR,e.getMessage());
      return modelAndView;
  }

  @ExceptionHandler(value = RuntimeException.class)
    @ResponseBody
    public Map<String,Object> runtimeException(RuntimeException e){
      e.printStackTrace();//将异常打印到控制台上
    //将错误信息放到map容器中
      return ToMap.getInstance().error().getMap();
  }


}
