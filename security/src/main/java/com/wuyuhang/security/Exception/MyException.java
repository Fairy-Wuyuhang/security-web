package com.wuyuhang.security.Exception;

/**
 * @author diao 2022/2/25
 */
public class MyException extends RuntimeException{
    public MyException(String message) {
        super(message);
    }
}
