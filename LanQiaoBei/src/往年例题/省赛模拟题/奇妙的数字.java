package 往年例题.数据结构训练;

import java.util.HashSet;

/**
 * @author diao 2022/3/16
 */
//求一些数的平方+立方和将0-9全部用了一遍
public class 奇妙的数字 {
    public static void main(String[] args) {
        //1.首先明确10位数,所以说最大为100
        for(int i=1;i<100;i++){

            //2.将一个数的平方和立方和放在一个字符串中
            String str=Math.pow(i,2)+""+Math.pow(i,3)+"";

            //3.构造一个Hashset,将字符串放入，然后判断
            HashSet<Character> set = new HashSet<>();
            for (int j=0;j<str.length();j++){
                set.add(str.charAt(j));
            }
            //4.因为set中有去重的效果，所以我们只需要判断长度==10即可
            if(set.size()==10){
                System.out.println(i);
            }else{
                continue;
            }
        }
    }
}
