package 往年例题.数据结构训练;

/**
 * @author diao 2022/3/19
 */
//考验了/和%的综合运用
public class 带9的个数 {
    public static void main(String[] args) {
        find();
    }

    public static void find(){
        int count=0;

        //开始遍历数字
      A:  for(int i=1;i<=2019;i++){
            int temp=i;
            while(temp!=0){
                int b = temp % 10;
                if(b==9){
                    count++;
                    //找到一个数字有9了，后面就不执行了，直接下一个循环
                    continue A;
                }
                //个位没有9，到十位...
                temp/=10;

            }
        }
        System.out.println(count);
    }
}
