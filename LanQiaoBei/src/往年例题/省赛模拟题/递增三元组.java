package 往年例题.数据结构训练;

import java.util.Scanner;

/**
 * @author diao 2022/3/19
 */
public class 递增三元组 {
    static int count=0;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n=scanner.nextInt();
        int[]nums=new int[n];

        for(int i=0;i<n;i++){
            nums[i] = scanner.nextInt();
        }
        scanner.close();

        //1.首先判断长度是否大于三
        if(n<3){
            System.out.println("没有三元组中心");
        }
        //2.先排序
        for(int i=0;i<nums.length;i++){
           for(int j=1;j<nums.length;j++){
               if(nums[j]<nums[j-1]){
                   int temp=nums[j-1];
                   nums[j-1]=nums[j];
                   nums[j]=temp;
               }
           }
        }

        //3.寻找
           for(int j=1;j<nums.length-1;j++){
               if(nums[j]>nums[j-1]&&nums[j]<nums[j+1]){
                   count++;
               }
           }
        System.out.println(count);


    }
}
