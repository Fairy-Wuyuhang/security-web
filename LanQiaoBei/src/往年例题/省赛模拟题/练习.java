package 往年例题.数据结构训练;

/**
 * @author diao 2022/3/19
 */
public class 练习 {
    public static void main(String[] args) {

    }

    public static void find(){
        int count=0;

       A: for(int i=1;i<=2019;i++){
            //对每一个数字进行判断，看是否含有9（每一个位置都需要遍历到）
            //所以说我们会改变i值，所以需要中间变量
            int temp=i;
            while(temp!=0){
                int b=temp%10;
                if(b==9){
                    count++;
                    continue A;
                }
                //个位数无9进位
                temp/=10;
            }
        }
        System.out.println(count);
    }
}
