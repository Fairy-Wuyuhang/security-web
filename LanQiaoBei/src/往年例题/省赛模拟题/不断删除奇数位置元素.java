package 往年例题.数据结构训练;

/**
 * @author diao 2022/3/17
 */
public class 不断删除奇数位置元素 {
    public static void main(String[] args) {

    }

    public static void delete(char[]data){
        int len=data.length;
        int temp=0;//设置一个变量，以便更新数组中的值

        //2.删除奇数位置元素直至数组中只剩下一个元素
        while(len!=1){
            //2.1遍历只保留偶数项
            for(int i=0;i<len;i+=2){
                data[temp++]=data[i];
            }
            //2.2更新len数组长度
            len=temp;
        }
        System.out.println(data[0]);
    }
}
