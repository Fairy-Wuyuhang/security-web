package 往年例题;

import java.util.Scanner;

/**
 * @author diao 2022/3/19
 */
//找到三个递增中间那位，问：多少个
public class 递增三元组 {
    public static void main(String[] args) {
      int[]data={2,1,4,5,6,3,8};
        sort(data);
        for(int i=0;i< data.length;i++){
            System.out.print(data[i]+" ");
        }
    }

    public static void sort(int[]data){
        for(int i=0;i< data.length;i++){
           for(int j=1;j< data.length;j++){
               if(data[j]<data[j-1]){
                   int temp=data[j-1];
                   data[j-1]=data[j];
                   data[j]=temp;
               }
           }
        }
    }

}
