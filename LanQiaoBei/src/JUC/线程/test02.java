package JUC;

import java.util.Arrays;

/**
 * @author diao 2022/3/31
 */

public class test02 {
    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread() {
            public void run() {
                System.out.println("开始");
            }
        };
        t1.start();
        t1.wait(100);
        System.out.println("正在运行");
    }
}
