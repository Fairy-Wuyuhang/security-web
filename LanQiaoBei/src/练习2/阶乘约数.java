package 练习2;

import 整数的基本性质.BIgInteger练习;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * @author diao 2022/3/23
 */
public class 阶乘约数 {
    public static void main(String[] args) {
        Long input= Long.valueOf(100);
        BigInteger ONE = new BigInteger("1");
        //开始阶乘，将input这个数一直递减
        while(input>0){
           ONE=ONE.multiply(BigInteger.valueOf(input));
            input--;
        }
        System.out.println(ONE);
        Long aLong = Cal_Count(ONE);
        System.out.println("有"+aLong+"个约数");
    }

    /**
     * 这里我们暴力遍历
     * @param n：传入一个BigInteger类型的数
     * 我们创建一个Long类型，将BigInteger转为Long进行遍历
     * @return
     */
    public static BigInteger Cal_Count(BigInteger n){
        BigInteger a=BigInteger.valueOf(0);
        BigInteger count= BigInteger.valueOf(0);
        for(BigInteger i=a;i.compareTo(n)==-1;i.add(BigInteger.ONE)){
            if(n.mod(i).compareTo(BigInteger.ZERO)==0){
                count.add(BigInteger.ONE);
            }
        }
        return count;
    }
}
