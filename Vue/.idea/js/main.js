/*
  请求地址:http://wthrcdn.etouch.cn/weather_mini
  请求方法:get
  请求参数:city(城市名)
  响应内容:天气信息

  1. 点击回车
  2. 查询数据
  3. 渲染数据
  */

var app = new Vue({
   el:"#app",
    data:{
        city:'',
        weatherList:[]
    },
   methods:{
       searchWeather:function () {
           //设置中间变量以便获取服务器返回的内容
           var that=this;

           // console.log('天气查询');
           //当输入栏回车触发searchweather方法,v-model会将数据给到data中的city（这是与之对应的）
           //然后通过this.city将当前城市进行查询，通过then()方法，服务器将数据返回
           axios.get('http://wthrcdn.etouch.cn/weather_mini?city='+this.city)
               .then(function (response) {
                       // console.log(response)
                   //将服务器响应的数据赋值给中间变量
                   that.weatherList=response.data.data.forecast
                   })
       },
       changeCity:function (city) {
           this.city=city;
           this.searchWeather();
       }
   },

});